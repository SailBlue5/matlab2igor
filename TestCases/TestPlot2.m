function [ Handle ] = TestPlot2()
%TESTPLOT2 Creates a test plot for ToIgor() and returns the handle.

Handle = figure;
title('ToIgor Test Plot 2: SubPlots');
hold all;
%Create plots
subplot(2,2,1)
plot(1:12, (1:12).^2, 'r-','LineWidth',2);
xlabel('Time (s)');
ylabel('Money Earned');
title('SubPlot 1');
months=['J';'F';'M';'A';'M';'J';'J';'A';'S';'O';'N';'D'];
set(gca,'XTick',1:12,'XTickLabel',months);
grid on;

subplot(2,2,2)
plot(1:100, sin(1:100));
xlabel('Time (s)');
ylabel('Sine');
title('SubPlot 2');


subplot(2,2,3)
plot(1:100, cos(1:100));
xlabel('Time (s)');
ylabel('Cosine');
title('SubPlot 3');

subplot(2,2,4)
semilogy(1:100, log(1:100));
xlabel('Time (s)');
ylabel('log');
title('SubPlot 4');
grid minor;
end
