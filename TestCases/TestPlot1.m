function [ Handle ] = TestPlot1()
%TESTPLOT1 Creates a test plot for ToIgor() and returns the handle.

Handle = figure;
hold all;
%Create plots
plot(1:100, 20*(1:100).^2, 'r-','LineWidth',2);
plot(1:100, (1:100).^2.3, 'b*');
plot(1:100, (1:100).^2.6, 'gsquare');
plot(1:100, (1:100).^2.8, 'k--','LineWidth',3);
plot(1:100, 1000*(1:100).^.9, 'm.-');
plot(1:100, 1000*(1:100), 'ydiamond');
xlabel('Time (s)');
ylabel('Money Earned');
title('ToIgor Test Plot 1');

end

